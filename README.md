#CS Devices DEV Tool

This project was created as a tool to keep track of our connected services devices

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the server
Open [http://localhost:8080](http://localhost:8080) to view it in the browser.

### `npm start-react`

Runs the react-app
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
You will also see any lint errors in the console.

### `npm run build`

Builds the app for production to the `build` folder.<br>
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.<br>
The server will serve you the file when opening http://localhost:8080

### `npm test`

Launches the test runner in the interactive watch mode.<br>
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.