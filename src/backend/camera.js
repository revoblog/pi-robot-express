const raspividStream = require('raspivid-stream');

exports.startVideoStream = (app) => {
    app.ws('/video-stream', (ws, req) => {
        console.log('Client connected');

        ws.send(JSON.stringify({
            action: 'init',
            width: '960',
            height: '540'
        }));

        var videoStream = raspividStream({ rotation: 180 });

        videoStream.on('data', (data) => {
            ws.send(data, { binary: true }, (error) => { if (error) console.error(error); });
        });

        ws.on('close', () => {
            console.log('Client left');
            videoStream.removeAllListeners('data');
        });
    });
};

