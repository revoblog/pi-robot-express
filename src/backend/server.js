const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();
const Gpio = require('pigpio').Gpio;
const wss = require('express-ws')(app);

const watchHCSR04 = require('./distanceSensor.js');
const camera = require('./camera.js');


app.use((req, res, next) => {
    console.info(`${req.method} ${req.originalUrl}`);
    next();
});
app.use(cors());
app.use(express.static(path.join(__dirname, '../../build')));
watchHCSR04.watchHCSR04();
camera.startVideoStream(app);

const leftMotor = new Gpio(7, {mode: Gpio.OUTPUT});
const rightMotor = new Gpio(9, {mode: Gpio.OUTPUT});

const leftMotorBackwards = new Gpio(8, {mode: Gpio.OUTPUT});
const rightMotorBackwards = new Gpio(10, {mode: Gpio.OUTPUT});

app.get('/ping', function (req, res) {
    return res.send('pong');
});

app.get('/move/:direction', function (req, res) {
    const direction = req.params.direction;

    leftMotor.digitalWrite(0);
    rightMotor.digitalWrite(0);
    leftMotorBackwards.digitalWrite(0);
    rightMotorBackwards.digitalWrite(0);

    if (direction === 'forward') {
        leftMotor.digitalWrite(1);
        rightMotor.digitalWrite(1);
    } else if (direction === 'backward') {
        leftMotorBackwards.digitalWrite(1);
        rightMotorBackwards.digitalWrite(1);
    } else if (direction === 'left') {
        rightMotor.digitalWrite(1);
    } else if (direction === 'right') {
        leftMotor.digitalWrite(1);
    }

    return res.send(204);
});

app.listen(8081);