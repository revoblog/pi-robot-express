export const getHost = () => {
    return `//${window.location.hostname}:${window.location.port}`
};