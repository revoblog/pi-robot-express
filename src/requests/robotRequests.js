import axios from 'axios';
import { getHost } from '../helpers/utilities';



export const moveRobot = (direction) => {
    return axios({
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Cache-Control': 'no-cache'
        },
        url: `${getHost()}/move/${direction}`,
    });
};