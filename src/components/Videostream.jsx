import React, { useEffect, useRef } from 'react';
import WSAvcPlayer from 'ws-avc-player'
import { getHost } from '../helpers/utilities';

export const Videostream = () => {

    const streamHtmlElement = useRef(null);

    useEffect((() => {
        const wsavc = new WSAvcPlayer(streamHtmlElement.current, "webgl");
        streamHtmlElement.current.appendChild(wsavc.AvcPlayer.canvas);
        var protocol = window.location.protocol === "https:" ? "wss:" : "ws:";
        wsavc.connect(protocol + getHost() + '/video-stream');
    }));


    return <>
            <div className={'videostream-camera'} ref={streamHtmlElement}/>
        </>
};