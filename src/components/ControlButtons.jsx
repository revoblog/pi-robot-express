import React, { useEffect } from 'react';
import { moveRobot } from '../requests/robotRequests';

export const ControlButtons = () => {

    useEffect((() => {
        document.addEventListener('keydown', function(event) {
            if(event.key === 'ArrowLeft') {
                moveRobot('left');
            } else if(event.key === 'ArrowRight') {
                moveRobot('right');
            } else if (event.key === 'ArrowUp') {
                moveRobot('forward');
            } else if (event.key === 'ArrowDown') {
                moveRobot('backward');
            } else {
                moveRobot('stop');
            }
        });
    }));


    return <>
        <button onClick={() => moveRobot('forward')}>&#9651;</button>

        <div className={"btn-group"}>
            <button onClick={() => moveRobot('left')}>&#8678;</button>
            <button onClick={() => moveRobot('backward')}>&#9661;</button>
            <button onClick={() => moveRobot('right')}>&#8680;</button>
        </div>
        <button className="btn-stop" onClick={() => moveRobot('stop')}>Stop</button>
    </>;
};