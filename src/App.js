import React from 'react';
import './App.css';

import { ControlButtons } from './components/ControlButtons';
import { Videostream } from './components/Videostream';

function App() {
  return (
    <div className="App">
        <Videostream />
        <ControlButtons/>
    </div>
  );
}

export default App;
