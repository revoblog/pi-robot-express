const express = require('express');
const cors = require('cors');
const path = require('path');
const app = express();
const _ = require("lodash");
var gpio = require('rpi-gpio');
const { exec } = require('child_process');

app.use((req, res, next) => {
    console.info(`${req.method} ${req.originalUrl}`);
    next();
});
app.use(cors());
app.use(express.static(path.join(__dirname, 'build')));

gpio.setMode(gpio.MODE_BCM);
gpio.setup(7, gpio.DIR_OUT, console.log);
gpio.setup(8, gpio.DIR_OUT, console.log);
gpio.setup(9, gpio.DIR_OUT, console.log);
gpio.setup(10, gpio.DIR_OUT, console.log);


app.get('/ping', function (req, res) {
    return res.send('pong');
});

app.get('/move/:direction', function (req, res) {
    const direction = req.params.direction;

    if (direction === 'forward') {
        gpio.write(7, true, console.log);
        gpio.write(8, false, console.log);
        gpio.write(9, true, console.log);
        gpio.write(10, false, console.log);
    } else if (direction === 'backward') {
        gpio.write(7, false, console.log);
        gpio.write(8, false, console.log);
        gpio.write(9, false, console.log);
        gpio.write(10, false, console.log);
    } else if (direction === 'left') {
        gpio.write(7, false, console.log);
        gpio.write(8, true, console.log);
        gpio.write(9, true, console.log);
        gpio.write(10, false, console.log);
    } else if (direction === 'right') {
        gpio.write(7, true, console.log);
        gpio.write(8, false, console.log);
        gpio.write(9, false, console.log);
        gpio.write(10, false, console.log);
    }
});

app.get('/camera', function (req, res) {
    exec('raspivid -t 0 -l -o tcp://0.0.0.0:3333');
});


app.listen(process.env.PORT || 8080);